package com.cms.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cms.model.Deal;

@Repository
public class DealDao {

  @PersistenceContext
  private EntityManager entityManager;

  public Deal getById(int id) {
    return entityManager.find(Deal.class, id);
  }

  @SuppressWarnings("unchecked")
  public List<Deal> getAll() {
    return entityManager.createQuery("select d from Deal d").getResultList();
  }

  public void save(Deal deal) {
    entityManager.persist(deal);
  }

}
