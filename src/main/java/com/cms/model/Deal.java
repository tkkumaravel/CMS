package com.cms.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONArray;
import org.json.JSONObject;

import flexjson.JSON;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

@Entity
@Table(name = "deals")
public class Deal implements Serializable {

  private static final long serialVersionUID = 1339334963242608614L;
  private Integer id;
  private String name;
  private Double outsidePrice;
  private Double originalDealPrice;
  private Integer maxQuantity;
  private Integer maxQuantityPerUser;
  private String termsOfUse;
  private String redemptionInstructions;
  private String comment;
  private Date dateCreated;
  private Date dateModified;
  private Date autoPublishDate;
  private Date publishedAt;
  private Date expiryDate;
  private Date archivedAt;
  private Date deletedAt;

  public static final DateTransformer DATE_TRANSFORMER = new DateTransformer("dd-MMM-yyyy");

  public Deal() {
  }

  public Deal(Integer usId, String usName, String usEmail) {
    this();
    this.id = usId;
    this.name = usName;
    this.redemptionInstructions = usEmail;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", unique = true, nullable = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Column(name = "name")
  @JSON(name = "title")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "outside_price")
  @JSON(name = "original_price")
  public Double getOutsidePrice() {
    return outsidePrice;
  }

  public void setOutsidePrice(Double outsidePrice) {
    this.outsidePrice = outsidePrice;
  }

  @Column(name = "original_deal_price")
  @JSON(name = "discounted_price")
  public Double getOriginalDealPrice() {
    return originalDealPrice;
  }

  public void setOriginalDealPrice(Double originalDealPrice) {
    this.originalDealPrice = originalDealPrice;
  }

  @Column(name = "max_quantity")
  @JSON(name = "max_quantity")
  public Integer getMaxQuantity() {
    return maxQuantity;
  }

  public void setMaxQuantity(Integer maxQuantity) {
    this.maxQuantity = maxQuantity;
  }

  @Column(name = "max_quantity_per_user")
  @JSON(name = "max_quantity_per_user")
  public Integer getMaxQuantityPerUser() {
    return maxQuantityPerUser;
  }

  public void setMaxQuantityPerUser(Integer maxQuantityPerUser) {
    this.maxQuantityPerUser = maxQuantityPerUser;
  }

  @Column(name = "terms_of_use")
  @JSON(name = "terms_of_use")
  public String getTermsOfUse() {
    return termsOfUse;
  }

  public void setTermsOfUse(String termsOfUse) {
    this.termsOfUse = termsOfUse;
  }

  @Column(name = "redemption_instructions")
  @JSON(name = "redemption_instructions")
  public String getRedemptionInstructions() {
    return redemptionInstructions;
  }

  public void setRedemptionInstructions(String redemptionInstructions) {
    this.redemptionInstructions = redemptionInstructions;
  }

  @Column(name = "comment")
  @JSON(name = "description")
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Column(name = "date_created", insertable = false, updatable = false)
  @JSON(name = "date_created")
  public Date getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }

  @Column(name = "date_modified", insertable = false, updatable = false)
  @JSON(name = "date_modified")
  public Date getDateModified() {
    return dateModified;
  }

  public void setDateModified(Date dateModified) {
    this.dateModified = dateModified;
  }

  @Column(name = "auto_publish_date")
  @JSON(name = "auto_publish_date")
  public Date getAutoPublishDate() {
    return autoPublishDate;
  }

  public void setAutoPublishDate(Date autoPublishDate) {
    this.autoPublishDate = autoPublishDate;
  }

  @Column(name = "published_at")
  @JSON(name = "published_at")
  public Date getPublishedAt() {
    return publishedAt;
  }

  public void setPublishedAt(Date publishedAt) {
    this.publishedAt = publishedAt;
  }

  @Column(name = "expiry_date")
  @JSON(name = "redemption_expiry_date")
  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  @Column(name = "archived_at")
  @JSON(name = "archived_at")
  public Date getArchivedAt() {
    return archivedAt;
  }

  public void setArchivedAt(Date archivedAt) {
    this.archivedAt = archivedAt;
  }

  @Column(name = "deleted_at")
  @JSON(name = "deleted_at")
  public Date getDeletedAt() {
    return deletedAt;
  }

  public void setDeletedAt(Date deletedAt) {
    this.deletedAt = deletedAt;
  }

  public JSONObject toJson(String... includedFields) {
    JSONObject json = new JSONObject((new JSONSerializer().transform(DATE_TRANSFORMER, Date.class)).include(includedFields).exclude("class").serialize(this));
    return json;
  }

  public JSONObject toJson() {
    JSONObject json = new JSONObject((new JSONSerializer().transform(DATE_TRANSFORMER, Date.class)).include("*").exclude("class").serialize(this));
    return json;
  }

  public static JSONArray toJson(Collection<Deal> collection, String... includedFields) {
    JSONArray array = new JSONArray();
    for (Deal item : collection) {
      array.put(item.toJson(includedFields));
    }
    return array;
  }

  public static JSONArray toJson(Collection<Deal> collection) {
    JSONArray array = new JSONArray();
    for (Deal item : collection) {
      array.put(item.toJson());
    }
    return array;
  }

  public static Deal fromJson(JSONObject json) {
    Deal deal = new JSONDeserializer<Deal>().use(Date.class, DATE_TRANSFORMER).deserialize(json + "", Deal.class);
    return deal;
  }

  @Override
  public boolean equals(Object obj) {
    return obj != null && this != null && obj instanceof Deal && this.getId() != null && ((Deal) obj).getId() != null
        && this.getId().intValue() == ((Deal) obj).getId().intValue();
  }

  @Override
  public int hashCode() {
    return this.getId() == null ? 31 : this.getId().intValue();
  }

}
