package com.cms.exception;

import java.util.LinkedHashSet;

import com.cms.exception.CMSMessage.ErrorLevel;

public class CMSException extends Exception {

  private static final long serialVersionUID = -1079660821679161403L;
  private LinkedHashSet<CMSMessage> hfhDealsMessages = new LinkedHashSet<CMSMessage>();

  public CMSException(Exception exception) {
    super(exception);
  }

  public CMSException(LinkedHashSet<CMSMessage> hfhDealsMessages) {
    super(hfhDealsMessages.toString());
    this.hfhDealsMessages = hfhDealsMessages;
  }

  public CMSException(String message, LinkedHashSet<CMSMessage> hfhDealsMessages) {
    super(message);
    this.hfhDealsMessages = hfhDealsMessages;
  }

  public CMSException(CMSMessage hfhDealsMessage) {
    super();
    add(hfhDealsMessage);
  }

  public CMSException(Exception exception, CMSMessage hfhDealsMessage) {
    super(exception);
    add(hfhDealsMessage);
  }

  public CMSException(ErrorLevel level, Integer code, String message) {
    add(new CMSMessage(level, code, message));
  }
  
  public void add(CMSMessage hfhDealsMessage) {
    hfhDealsMessages.add(hfhDealsMessage);

  }

  public LinkedHashSet<CMSMessage> getHFHDealsMessages() {
    return hfhDealsMessages;
  }

  public CMSMessage getTopHFHDealsMessage() {
    return hfhDealsMessages != null ? hfhDealsMessages.size() > 0 ? hfhDealsMessages.iterator().next() : null : null;
  }

  public String getTopMessage() {
    return (hfhDealsMessages.isEmpty() ? "" : hfhDealsMessages.iterator().next().getMessage());
  }

  @Override
  public String getMessage() {
    StringBuffer sb = new StringBuffer();
    if (hfhDealsMessages != null) {
      for (CMSMessage sm : hfhDealsMessages) {
        sb.append(sm);
      }
    }
    return "HFHDeals Message:\n" + sb.toString() + "\nException Message:\n" + super.getMessage();
  }

  public String getExceptionMessage(Boolean trim) {
    try {
      return trim ? super.getMessage().substring(0, super.getMessage().indexOf("\n", 100)) : super.getMessage();
    } catch (Exception e) {
      return super.getMessage();
    }
  }

  @Override
  public void printStackTrace() {
    StringBuffer sb = new StringBuffer();
    if (hfhDealsMessages != null) {
      for (CMSMessage sm : hfhDealsMessages) {
        sb.append(sm);
      }
    }
    System.out.println(sb + "\n Exception StackTrace: \n");
    super.printStackTrace();
  }
}
