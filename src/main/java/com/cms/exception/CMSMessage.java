package com.cms.exception;

import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONObject;
import flexjson.JSONSerializer;

public class CMSMessage {

  private Class<?> entityClass;
  private Integer entityId;
  private Object entityDetails;
  private Integer code;
  private ErrorLevel level;
  private String message;

  public static final Integer SUCCESS = 10;
  public static final Integer DEPRECATED = 15;
  public static final Integer DUPLICATE = 20;
  public static final Integer NOT_FOUND = 30;
  public static final Integer RESOURCE_LOCKED = 31;
  public static final Integer NO_RESPONSE = 40;
  public static final Integer DB_ERROR = 50;
  public static final Integer GENERIC_ERROR = 51;
  public static final Integer FILE_ERROR = 52;
  public static final Integer BAD_INPUT = 60;
  public static final Integer DEPENDENT = 70;

  
  public enum ErrorLevel {
    Info, Message, Debug, Error, Fatal
  }
  
  public CMSMessage() {
  }

  public CMSMessage(ErrorLevel level, Integer code, Class<?> entityClass, Integer entityId, String message) {
    this.entityClass = entityClass;
    this.entityId = entityId;
    this.entityDetails = null;
    this.code = code;
    this.level = level;
    this.message = message;
  }

  public CMSMessage(ErrorLevel level, Integer code, Class<?> entityClass, Object entityDetails, String message) {
    this.entityClass = entityClass;
    this.entityDetails = entityDetails;
    this.entityId = null;
    this.code = code;
    this.level = level;
    this.message = message;
  }

  public CMSMessage(ErrorLevel level, Integer code, String message) {
    this.code = code;
    this.message = message;
    this.level = level;
  }

  public CMSMessage(ErrorLevel level, String message) {
    this.message = message;
    this.level = level;
  }

  public CMSMessage(Integer code, String message) {
    this.code = code;
    this.message = message;
  }

  public Class<?> getEntityClass() {
    return entityClass;
  }

  public void setEntityClass(Class<?> entityClass) {
    this.entityClass = entityClass;
  }

  public Integer getEntityId() {
    return entityId;
  }

  public void setEntityId(Integer entityId) {
    this.entityId = entityId;
  }

  public Integer getCode() {
    return code;
  }

  public void setCode(Integer code) {
    this.code = code;
  }

  public ErrorLevel getLevel() {
    return level;
  }

  public void setLevel(ErrorLevel level) {
    this.level = level;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getEntityDetails() {
    return entityDetails;
  }

  public void setEntityDetails(Object entityDetails) {
    this.entityDetails = entityDetails;
  }

  @Override
  public String toString() {
    return "errorLevel: " + level + ", code: " + code + ", entityClass: " + entityClass + ", entityId:" + entityId + ", entityDetails:" + entityDetails
        + ", message:" + message;
  }

  public JSONObject toJson() {
    JSONObject json = new JSONObject(new JSONSerializer().exclude("hfhDealsMessagesStr").exclude("*.class").include("*").deepSerialize(this));
    if (entityDetails instanceof JSONObject || entityDetails instanceof JSONArray) {
      json.put("entityDetails", entityDetails);
    }
    return json;
  }

  public static JSONArray toJson(Collection<CMSMessage> collection) {
    JSONArray array = new JSONArray();
    for (CMSMessage item : collection) {
      array.put(item.toJson());
    }
    return array;
  }


}
