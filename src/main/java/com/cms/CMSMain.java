package com.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CMSMain {
  public static void main(String[] args) {
    SpringApplication.run(CMSMain.class, args);
  }
}