package com.cms.service;

import java.util.List;

import com.cms.model.Deal;

public interface DealService {

  public Deal getById(Integer id);

  public List<Deal> getAll();
  
  public Deal save(Deal deal);


  /*
  public Deal getByName(String name);
  
  public Deal update(Deal deal) throws HFHDealsException;
  
  public JSONArray getAllJson();
  
  public void delete(Deal deal);
  */
}
