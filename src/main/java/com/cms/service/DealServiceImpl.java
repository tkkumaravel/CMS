package com.cms.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cms.dao.DealDao;
import com.cms.model.Deal;

@Service("dealService")
@Transactional
public class DealServiceImpl implements DealService {

  @Autowired
  DealDao dealDao;

  private static final Log LOG = LogFactory.getLog(DealServiceImpl.class);

  public Deal getById(Integer id) {
    LOG.debug("getById for id [" + id + "]");
    Deal deal = dealDao.getById(id);
    return deal;
  }

  @Override
  public List<Deal> getAll() {
    LOG.debug("getAll");
    return dealDao.getAll();
  }

  @Override
  public Deal save(Deal deal) {
    dealDao.save(deal);
    return deal;
  }

  /*
  @Override
  public Deal getByName(String dealName) {
    LOG.debug("getByName for dealName [" + dealName + "]");
    return dealDao.getByDealName(dealName);
  }
  
  @Override
  public List<Deal> getAll() {
    LOG.debug("getAll");
    return dealDao.getAll();
  }
  
  @Override
  public Deal save(Deal deal) throws HFHDealsException {
    if (deal.getId() != null && deal.getId() > 0) {
      Deal dealDB = dealDao.getById(deal.getId());
      if (dealDB == null) {
      }
    } else {
      if (HFHDealsStringUtils.isNotNullAndEmpty(deal.getName())) {
        Deal dealDBByName = dealDao.getByDealName(deal.getName());
        if (dealDBByName != null) {
        }
      }
    }
    try {
      return dealDao.save(deal);
    } catch (HFHDealsException se) {
      throw se;
    }
  }
  
  @Override
  public Deal update(Deal deal) throws HFHDealsException {
    if (deal.getId() != null && deal.getId() > 0) {
      Deal dealDB = dealDao.getById(deal.getId());
      if (dealDB == null) {
      }
    } else {
      if (HFHDealsStringUtils.isNotNullAndEmpty(deal.getName())) {
        Deal dealDBByName = dealDao.getByDealName(deal.getName());
        if (dealDBByName != null) {
        }
      }
    }
    try {
      return dealDao.save(deal);
    } catch (HFHDealsException se) {
      throw se;
    }
  }
  
  @Override
  public JSONArray getAllJson() {
    return Deal.toJson(getAll());
  }
  
  @Override
  public void delete(Deal deal) {
    dealDao.delete(deal);
  }
  */
}
