# Halal Food Hunt Deals BE:: Backend REST APIs

The application is Java Spring Boot based REST API webapp to support the HFHDeals Frontend, HFHMain & other mobile apps.

## Getting Started

These instructions will guide you how to access the deployed service.
Furthermore, (if needed) to get a copy of the project up and running on your local machine for development and white box testing purposes.

### Prerequisites

1. To access deployed application you would need Postman. Postman can be installed as a chrome extention/plugin. Or, it can be installed as a standalone application for Windows and Mac. The instructions below refer to standalone application for Windows.

[Get Postman](https://www.getpostman.com/apps)

2. Optionally, to deploy the application locally you would need Java 1.8 and Maven. Make sure java and mvn are in your classpath.
3. Optionally, to have better control on your data, you can host your own Mysql.

### Accessing Services

The application is hosted and deployed at AWS Servers.
In postman, configure the environment by adding the environment file (aws.postman_environment.json).
Environment can be managed by Gear button found on top right corner of Postman application, just below Sign In Section.
Next is to import API Endpoints (HFHDeals.postman_collection.json)


### Compiling Locally

Just run the build.bat file which builds the Fat Jar file.


### Running Locally

Just run run.bat
On Mac, do sh ./run.bat
To configure and change the database, access application.properties file.


