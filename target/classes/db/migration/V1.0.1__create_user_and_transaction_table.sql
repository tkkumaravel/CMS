create table user (
	id int(11) not null auto_increment primary key,
	username varchar(50) not null,
	password varchar(50) not null,
	email varchar(50) 
	);
CREATE TABLE transaction(
     transac_id int(11) NOT NULL AUTO_INCREMENT,
     amount int(30) unsigned NOT NULL,
     to_user varchar(50) NOT NULL,
     log_date timestamp NOT NULL DEFAULT NOW(),
     PRIMARY KEY  (transac_id)
 );
